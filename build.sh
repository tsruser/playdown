#!/usr/bin/env nix-shell
#!nix-shell --pure
#!nix-shell -p "haskellPackages.ghcWithPackages (p: with p; [lens wreq optparse-applicative])"
#!nix-shell -i bash

cd "${0%/*}"

deps=(lens wreq optparse-applicative)
main=Main
target=playdown

FLAGS="-outputdir $PWD/.ghc -i$PWD/src -o $PWD/$target"

for dep in ${deps[@]}; do
  FLAGS="$FLAGS -package $dep"
done

ghc $FLAGS $main

{-# LANGUAGE DataKinds, RecordWildCards, FlexibleInstances #-}

module Main
  ( main
  ) where

import Control.Applicative ((<|>))
import Control.Lens ((^.))
import Control.Monad (mapM)
import Data.ByteString.Lazy (writeFile)
import Data.Char (isAlpha, isDigit)
import Data.Maybe (fromMaybe, fromJust)
import Network.Wreq (get, responseBody)
import Options.Applicative
  ( (<**>)
  , argument
  , execParser
  , fullDesc
  , helper
  , info
  , metavar
  , str
  )
import Prelude hiding (writeFile)
import System.Environment (getArgs)
import System.FilePath ((<.>), takeExtension, makeValid)
import Text.ParserCombinators.ReadP hiding (get)

data Track =
  Track
    { duration :: Int
    , name :: String
    , url :: String
    }

instance Show Track where
  show (Track {..}) =
    unlines [ "Duration: " <> show duration
            , "Name:     " <> name
            , "URL:      " <> url
            ]

parse :: ReadP result -> String -> Maybe result
parse parser src =
  case [result | (result, "") <- readP_to_S parser src] of
    [] -> Nothing
    (ok:_) -> Just ok

number :: ReadP Int
number = read <$> munch1 isDigit

urlencoded :: ReadP String
urlencoded = munch1 $ \c -> isAlpha c || c == '%'

line :: ReadP String
line = munch1 (not . flip elem "\r\n")

newline :: ReadP ()
newline = const () <$> munch1 (flip elem "\r\n")

track :: ReadP Track
track = do
  string "#EXTINF:"
  duration <- number
  char ','
  name <- line
  newline
  url <- line
  return Track {..}

playlist :: ReadP [Track]
playlist = do
  string "#EXTM3U"
  newline
  many1 (track <* optional newline)

-- >>> putStrLn . fromMaybe "" $ (unlines . map show) <$> parse playlist (unlines [ "#EXTM3U", "#EXTINF:120,Hello, World!", "https://google.com/favicon.ico"])
-- Duration: 120
-- Name:     Hello, World!
-- URL:      https://google.com/favicon.ico

main :: IO ()
main = do
  filename <-
    execParser (info (argument str (metavar "Playlist") <**> helper) fullDesc)
  file <- readFile filename
  mapM downloadTrack . fromMaybe [] $ parse playlist file
  return ()

downloadTrack :: Track -> IO ()
downloadTrack (Track {..}) = do
  putStrLn $ "Downloading \"" <> filename <> "\"..."
  contents <- get url
  writeFile filename $ contents ^. responseBody
  where
    ext = takeExtension url
    filename = makeValid $ name <.> ext

